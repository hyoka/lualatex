# lualatex(lualatexに限らない) 実行用Dockerfile

## 使い方(Ubuntu)

1. [Docker](https://www.docker.com/) インストール

    Ubuntu 18.10 以下

    - [ドキュメント](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - コマンド
        
        ```bash
        sudo apt-get update
        sudo apt-get install \
            apt-transport-https \
            ca-certificates \
            curl \
            gnupg-agent \
            software-properties-common
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        sudo apt-key fingerprint 0EBFCD88
        sudo add-apt-repository \
            "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
            $(lsb_release -cs) \
            stable"
        sudo apt-get update
        sudo apt-get install docker-ce docker-ce-cli containerd.io
        ```

    Ubuntu 19.04

    - ↑でできなかったので以下のコマンドで

        ```bash
        curl -fsSL get.docker.com | CHANNEL=test sh
        ```

        `CHANNEL=test` を消せば他のバージョンでも使えそう



1. Docker使用前準備
    - 自身をdocker グループに追加する
    - コマンド

        ```bash
        sudo gpasswd -a [username] docker
        ```

    - 一旦再起動

1. lualatex用Dockerイメージをビルド
    - コマンド
    
        ```bash
        docker build -t lualatex .
        ```

1. 実際に使ってみる
    - コマンド
    
        ```bash
        cd [path to your project]
        docker run -u `id -u`:`id -g` --rm -it -v `pwd`:/workdir lualatex:latest lualatex [tex file]
        ```
