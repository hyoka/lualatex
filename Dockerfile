FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get -y install \
    texlive-full \
    && apt-get clean

RUN luaotfload-tool -v -vvv -u

RUN mkdir /workdir && chmod 0777 /workdir
RUN chmod -R 0777 /var/lib/texmf
WORKDIR /workdir
VOLUME ["/workdir"]
CMD ["bash"]